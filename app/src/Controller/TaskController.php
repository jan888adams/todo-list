<?php

namespace App\Controller;

use App\Entity\State;
use App\Entity\Task;
use App\Entity\User;
use App\Form\FilterOptionType;
use App\Form\SearchBarType;
use App\Form\TaskType;
use App\Service\EntityFinderInterface;
use App\Service\MailerServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/todo", name="task.")
 */
final class TaskController extends AbstractController
{
    const CREATED_SUCCESS_MESSAGE = 'TODO wurde erfolgreich angelegt';
    const UPDATE_SUCCESS_MESSAGE = 'TODO wurde erfolgreich aktualisier';
    const EMAIL_UPDATE_MESSAGE = 'Sie haben ein neues TODO zugewiesen bekommen';
    const DEFAULT_MESSAGE = '';

    const IS_LOGGED = true;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $databaseManager;

    /**
     * @var EntityFinderInterface
     */
    private EntityFinderInterface $finder;

    /**
     * @var string
     */
    private string $loggedUserEmail;

    /**
     * TaskController constructor.
     * @param EntityManagerInterface $databaseManager
     * @param EntityFinderInterface $finder
     * @param AuthenticationUtils $authenticationUtils
     */
    public function __construct(
        EntityManagerInterface $databaseManager,
        EntityFinderInterface $finder,
        AuthenticationUtils $authenticationUtils
    ) {
        $this->databaseManager = $databaseManager;
        $this->finder = $finder;
        $this->loggedUserEmail = $authenticationUtils->getLastUsername();
    }

    /**
     * @Route("/anlegen", name="addTask")
     * @param Request $request
     * @return Response
     */
    public function addTask(Request $request): Response
    {
        $task = new Task();
        $taskForm = $this->createForm(TaskType::class, $task);
        $taskForm->handleRequest($request);

        $message = self::DEFAULT_MESSAGE;

        if ($taskForm->isSubmitted() && $taskForm->isValid()) {
            $user = $this->finder->find(['email' => $this->loggedUserEmail], User::class);
            $task->setCreator($user);

            $this->databaseManager->persist($task);
            $this->databaseManager->flush();

            $message = self::CREATED_SUCCESS_MESSAGE;
        }

        return $this->render(
            'task/form.html.twig',
            [
                'isLogged' => true,
                'message' => $message,
                'taskForm' => $taskForm->createView(),
            ]
        );
    }

    /**
     * @Route("/task/{field?}", name="showTasks")
     * @param Request $request
     * @param string|null $field
     * @return Response
     */
    public function showTasks(Request $request, ?string $field): Response
    {
        $filterForm = $this->createFilterForm();
        $searchForm = $this->createForm(SearchBarType::class);

        $filterForm->handleRequest($request);
        $filter = [];

        if ($filterForm->isSubmitted() && $filterForm->isValid()) {
            $formData = $filterForm->getData();

            $filter = [
                'state' => $formData['state'],
                'editor' => $formData['editor'],
                'creator' => $formData['creator'],
            ];
        }

        $searchForm->handleRequest($request);
        $termToSearch = null;

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $formData = $searchForm->getData();
            $termToSearch = $formData['search'];
        }

        $orderBy = null;

        if ($field !== null) {
            $ascendingOrder  = 'ASC';
            $orderBy = [$field => $ascendingOrder];
        }

        $tasks = $this->finder->findBy(Task::class, $filter, $orderBy, $termToSearch);

        return $this->render(
            'task/overview.html.twig',
            [
                'isLogged' => self::IS_LOGGED,
                'tasks' => $tasks,
                'filterOptionForm' => $filterForm->createView(),
                'searchForm' => $searchForm->createView(),
                'orderFields' => ['field1' => 'title', 'field2' => 'editor', 'field3' => 'creator', 'field4' => 'state'],
            ]
        );
    }


    /**
     * @Route("/{title}", name="showTask")
     * @param $title
     * @return Response
     */
    public function showTask(string $title): Response
    {
        $task = $this->finder->find(['title' => $title], Task::class);

        return $this->render(
            'task/detail.html.twig',
            [
                'isLogged' => self::IS_LOGGED,
                'task' => $task,
            ]
        );
    }

    /**
     * @Route("/bearbeiten/{title}", name="editTask")
     * @param string $title
     * @param Request $request
     * @param MailerServiceInterface $mailer
     * @return Response
     */
    public function editTask(string $title, Request $request, MailerServiceInterface $mailer): Response
    {
        $task = $this->finder->find(['title' => $title], Task::class);
        $lastEditor = $task->getEditor();

        $defaultOptions = [
            'title' => $task->getTitle(),
            'description' => $task->getDescription(),
            'editor' => $lastEditor,
            'state' => $task->getState(),
        ];

        $message = self::DEFAULT_MESSAGE;

        $taskForm = $this->createForm(TaskType::class, $task, $defaultOptions);
        $taskForm->handleRequest($request);

        if ($taskForm->isSubmitted() && $taskForm->isValid()) {
            $user = $this->finder->find(['email' => $this->loggedUserEmail], User::class);
            $task->setCreator($user);

            $this->databaseManager->persist($task);
            $this->databaseManager->flush();

            if ($task->getEditor() !== $lastEditor && $task->getState() !== 'Erledigt') {
                $mailer->send($task->getEditor(), $task->getCreator(), self::EMAIL_UPDATE_MESSAGE);
            }

            $message = self::UPDATE_SUCCESS_MESSAGE;
        }

        return $this->render(
            'task/edit.html.twig',
            [
                'isLogged' => self::IS_LOGGED,
                'message' => $message,
                'taskForm' => $taskForm->createView(),
            ]
        );
    }

    /**
     * @return FormInterface
     */
    private function createFilterForm(): FormInterface
    {
        $states = $this->finder->findBy(State::class);
        $users = $this->finder->findBy(User::class);

        $FilterSelectOptions = [
            'editors' => $users,
            'creators' => $users,
            'states' => $states,
        ];

        return $this->createForm(FilterOptionType::class, null, $FilterSelectOptions);
    }
}
