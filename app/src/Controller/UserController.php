<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


final class UserController extends AbstractController
{
    const REGISTRATION_SUCCESS_MESSAGE = 'sie konnten sich erfolgreich registrieren';
    const DEFAULT_MESSAGE = '';

    /**
     * @Route("/registrieren", name="addUser")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function addUser(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $message = self::DEFAULT_MESSAGE;

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $passwordEncoder->encodePassword($user, $user->getPassword())
            );

            $dbManager = $this->getDoctrine()->getManager();
            $dbManager->persist($user);
            $dbManager->flush();

            $message = self::REGISTRATION_SUCCESS_MESSAGE;
        }

        return $this->render(
            'user/form.html.twig',
            [
                'message' => $message,
                'registrationForm' => $form->createView()
            ]
        );
    }
}
