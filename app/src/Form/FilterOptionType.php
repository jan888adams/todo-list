<?php

namespace App\Form;

use App\Entity\State;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FilterOptionType
 * @package App\Form
 */
final class FilterOptionType extends AbstractType
{
    private const DEFAULT_PLACEHOLDER =  'kein Filter';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add(
                'editor',
                EntityType::class,
                [
                    'class' => User::class,
                    'placeholder' => self::DEFAULT_PLACEHOLDER,
                    'required' => false,
                    'choices' => $options['editors'],
                    'label' => 'Bearbeiter'

                ]
            )
            ->add(
                'creator',
                EntityType::class,
                [
                    'class' => User::class,
                    'placeholder' => self::DEFAULT_PLACEHOLDER,
                    'required' => false,
                    'choices' => $options['creators'],
                    'label' => 'Ersteller'
                ]
            )
            ->add(
                'state',
                EntityType::class,
                [
                    'class' => State::class,
                    'placeholder' => self::DEFAULT_PLACEHOLDER,
                    'required' => false,
                    'choices' => $options['states'],
                    'label' => 'Status'
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Filtern'
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'editors' => true,
                'creators' => true,
                'states' => true,
            ]
        );
    }
}
