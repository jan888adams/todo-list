<?php

namespace App\Form;

use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TaskType
 * @package App\Form
 */
final class TaskType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'Title',
                    'attr' => [
                        'placeholder' => $options['title'],
                    ],
                ]
            )
            ->add(
                'description',
                null,
                [
                    'label' => 'Beschreibung',
                    'attr' => [
                        'placeholder' => $options['description'],
                    ],
                ]
            )
            ->add(
                'targetDate',
                DateType::class,
                [
                    'label' => 'Fälligkeitsdatum'
                ]
            )
            ->add(
                'editor',
                null,
                [
                    'label' => 'Bearbeiter',
                    'attr' => [
                        'placeholder' => $options['editor'],
                    ],
                ]
            )
            ->add('state', null, [
                'label' => 'Status',
                'attr' => [
                    'placeholder' => $options['state']
                ]
            ] )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'hinzufügen'
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Task::class,
                'title' => false,
                'description' => false,
                'editor' => false,
                'state' => false
            ]
        );
    }
}
