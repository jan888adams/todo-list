<?php


namespace App\Repository;

interface SearchableInterface
{
    /**
     * @param array|null $termsToFind
     * @param array|null $termsToExclude ['entityField' => 'term']
     * @param array $termsThatContainsAtLeastOne
     * @return object[]|null
     */
    public function findByOccurrence(array $termsToFind = [], array $termsToExclude = [], array $termsThatContainsAtLeastOne = []): ?array;

}