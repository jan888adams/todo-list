<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository implements SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * @param array $termsToFind
     * @param array $termsToExclude
     * @param array $termsThatContainsAtLeastOne
     * @return Task[]|null
     */
    public function findByOccurrence(array $termsToFind = [], array $termsToExclude = [], array $termsThatContainsAtLeastOne = []): ?array
    {
        $queryBuilder = $this->createQueryBuilder('task');

        if($termsToFind !== []){
            foreach ($termsToFind as $term){
               $queryBuilder->andWhere('task.title LIKE :term OR task.description LIKE :term')
                    ->setParameter('term', '%'.$term.'%');
            }
        }

       if($termsToExclude != []){
           foreach ($termsToExclude as $term){
             $queryBuilder->andWhere('task.title NOT LIKE :term OR task.description NOT LIKE :term')
                   ->setParameter('term', '%'.$term.'%');

           }
       }

        if($termsThatContainsAtLeastOne != []){
            foreach ($termsThatContainsAtLeastOne as $term){
                $queryBuilder->orWhere('task.title LIKE :term OR task.description LIKE :term')
                    ->setParameter('term', '%'.$term.'%');

            }
        }

        $query = $queryBuilder->getQuery();
        $result = $query->getResult();

        if (is_array($result)){
            return $result;
        }

        return null;
    }

}
