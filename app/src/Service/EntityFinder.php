<?php

namespace App\Service;

use App\Repository\SearchableInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class EntityFinder
 * @package App\Service
 */
final class EntityFinder implements EntityFinderInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $manager;

    /**
     * Finder constructor.
     * @param EntityManagerInterface $databaseManager
     */
    public function __construct(EntityManagerInterface $databaseManager)
    {
        $this->manager = $databaseManager;
    }

    /**
     * @param array $criteria
     * @param string $className
     * @return object|null
     */
    public function find(array $criteria, string $className): ?object
    {
        return $this->manager->getRepository($className)
            ->findOneBy($criteria);
    }

    /**
     * @param string $className
     * @param array $filter
     * @param null $orderBy
     * @param string|null $term
     * @return object[]|null
     */
    public function findBy(string $className, array $filter = [], $orderBy = null, string $term = null): ?array
    {
        $repository = $this->manager->getRepository($className);

        foreach ($filter as $key => $value) {
            if ($value === null) {
                unset($filter[$key]);
            }
        }

        $entities = $repository->findBy($filter, $orderBy);

        if ($term !== null && $repository instanceof SearchableInterface) {
            $termsToFind = [];
            $termsToExclude = [];

            if (str_contains($term, '+')) {
                $termsToFind = explode('+', $term);
            } elseif (str_contains($term, '-')) {
                $termsToExclude = explode('-', $term);
            } elseif (str_contains($term, 'or')) {
                $termsToExclude = explode('or', $term);
            }else {
                $termsToFind[] = $term;
            }

            $entitiesDefinedByTerm = $repository->findByOccurrence($termsToFind, $termsToExclude);
            foreach ($entities as $entity) {
                $filter['id'] = $entity->getId();
            }

            $entities = array_uintersect(
                $entities,
                $entitiesDefinedByTerm,
                fn($entity, $entityDefinedByTerm) => $entity->getId() - $entityDefinedByTerm->getId()
            );
        }

        return $entities;
    }

}