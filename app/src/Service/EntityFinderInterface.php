<?php

namespace App\Service;

/**
 * Interface EntityFinderInterface
 * @package App\Service
 */
interface EntityFinderInterface
{
    /**
     * @param array $criteria
     * @param string $className
     * @return Object|null
     */
    public function find(array $criteria, string $className): ?Object;

    /**
     * @param string $className
     * @param array $filter
     * @param null $orderBy
     * @param string|null $term
     * @return Object[]|null
     */
    public function findBy(string $className, array $filter = [], $orderBy = null, string $term = null): ?array;



}