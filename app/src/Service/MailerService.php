<?php

namespace App\Service;

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Class MailerService
 * @package App\Service
 */
final class MailerService implements MailerServiceInterface
{
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * Mailer constructor.
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string $sendTo
     * @param string $sendFrom
     * @param string $text
     * @return bool
     */
    public function send(string $sendTo, string $sendFrom, string $text): bool
    {
        $mail = new Email();
        $mail->addFrom($sendFrom)
            ->addTo($sendTo)
            ->text($text);

        try {
            $this->mailer->send($mail);
            $wasMailSent = true;
        } catch (TransportExceptionInterface $e) {
            $wasMailSent = false;
        }

        return $wasMailSent;
    }
}