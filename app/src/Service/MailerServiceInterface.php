<?php


namespace App\Service;


/**
 * Interface MailerServiceInterface
 * @package App\Service
 */
interface MailerServiceInterface
{
    /**
     * @param string $sendTo
     * @param string $sendFrom
     * @param string $text
     * @return bool
     */
    public function send(string $sendTo, string $sendFrom, string $text): bool;

}