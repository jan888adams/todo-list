<?php

namespace src\Controller\IntegrationTests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    public function testAddTaskShouldReturnRegistrationTemplate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@email.com');

        $client->loginUser($testUser);
        $client->request('GET', '/todo/anlegen');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Neues ToDo hinzufügen');
    }

    public function testShowTasksShouldReturnOverviewTemplate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@email.com');

        $client->loginUser($testUser);
        $client->request('GET', '/todo/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Übersicht über alle offenen Todos');
    }

    public function testShowTaskShouldReturnDetailTemplate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@email.com');

        $client->loginUser($testUser);
        $client->request('GET', '/todo/Test%20Task');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Test Task');
    }

    public function testEditTaskShouldReturnEditTemplate()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@email.com');

        $client->loginUser($testUser);
        $client->request('GET', '/todo/editieren/Test%20Task');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Test Task');
    }


}
