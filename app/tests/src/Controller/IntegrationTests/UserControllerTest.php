<?php

namespace src\Controller\IntegrationTests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{

    public function testAddUserShouldReturnRegistrationTemplate()
    {
        $client = static::createClient();
        $client->request('GET', '/registrieren');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Registrieren sie sich');
    }


   /* public function testRegisterShouldReturnTemplateWithRegistrationSuccessMessageIfRegistrationIsSuccessful()
    {
        $client = static::createClient();
        $client->request('GET', '/registrieren');
        $client->submitForm(
            'registrieren',
            [
                'registrationForm[email]' => 'test@email.de',
                'registrationForm[password]' => 'password'
            ]
        );

        $this->assertSelectorTextContains('div', 'sie konnten sich erfolgreich registrieren');
    }

   */


}
