<?php

namespace src\Service\UnitTests;

use App\Entity\State;
use App\Service\EntityFinder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class EntityFinderTest extends TestCase
{

    private EntityFinder $entityFinder;
    public ObjectRepository $repository;

    public function setUp(): void
    {
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $this->entityFinder = new EntityFinder($objectManager);
        $this->repository = $this->createMock(ObjectRepository::class);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($this->repository);
    }

    public function testFindShouldReturnFalseWhenEntityWasNotFound()
    {
        $searchCriteria = ['id' => 2];

        $this->repository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $result = $this->entityFinder->find($searchCriteria, 'existingEntity');
        $expected = false;

        $this->assertEquals($expected, $result);
    }

    public function testFindShouldReturnAnEntityWhenEntityWasFound()
    {
        $searchCriteria = ['id' => 2];
        $entity = new State();

        $this->repository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($entity);

        $result = $this->entityFinder->find($searchCriteria, 'existingEntity');
        $expected = $entity;

        $this->assertEquals($expected, $result);
    }

    public function testFindAllShouldReturnFalseWhenNoEntitiesWasFound(){
        $searchCriteria = ['id' => 2];

        $this->repository->expects($this->any())
            ->method('findAll')
            ->willReturn(null);

        $result = $this->entityFinder->find($searchCriteria, 'existingEntity');
        $expected = false;

        $this->assertEquals($expected, $result);
    }

}
